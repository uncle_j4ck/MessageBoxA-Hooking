# What this Windows API Hooking
- **Windows API hooking** is a process allowing to intercept API function calls. This gives you the control over the way operating system or a piece of software behaves.

## TECHNICAL DETAILS
* Get memory address of the MessageBoxA function
* Read the first 6 bytes of the MessageBoxA - will need these bytes for unhooking the function
* Create a HookedMessageBox function that will be executed when the original MessageBoxA is called
* Get memory address of the HookedMessageBox
* Patch / redirect MessageBoxA to HookedMessageBox
* Call MessageBoxA. Code gets redirected to HookedMessageBox
* HookedMessageBox executes its code, prints the supplied arguments, unhooks the MessageBoxA and transfers the code control to the actual MessageBoxA

## DEMO

![hook](https://github.com/Zextus/MessageBoxA-Hooking/blob/main/demo.gif)
